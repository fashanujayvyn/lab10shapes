/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package liskov1;

/**
 *
 * @author jayvy
 */
import static org.junit.Assert.assertEquals;
public class ShapeDemo {
   
    public static void calculateArea(Rectangle r){
        
     r.setWidth(2);
     r.setHeight(3);
        
        assertEquals("Area calculations is incorrect" , r.getArea( ) , 6);
    }
    public static void main(String [] args){
        
     ShapeDemo.calculateArea(new Rectangle() );   
     
     ShapeDemo.calculateArea(new Square() );
        
    }
    
}
